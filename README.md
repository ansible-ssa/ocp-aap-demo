# OCP + AAP Demo

Install and configure Ansible Automation Platform Operator in Red Hat OpenShit.

### AAP Operator Variables

The following variables are defined:

|       Variable         |                   Definition                   |  Type  |
|:----------------------:|:----------------------------------------------:|:------:|
|    kubeconfig_path     | Kubeconfig file location                       | string |
|       aap_path         | Path with AAP manifests                        | string |
| aap_operator_namespace | AAP operator namespace                         | string |
|  aap_operator_path     | Path with manifests to install AAP operator    | string |

### AAP Controller Variables

The following variables are defined:

|                Variable                |                               Definition                                |  Type  |
|:--------------------------------------:|:-----------------------------------------------------------------------:|:------:|
|             aap_manifest               | Ansible Automation Subscription Manifest file(*encode base64*)          | string |
|      aap_controller_instance_path      | Path with manifests to install Automation Controller instance           | string |
| aap_controller_instance_configuration  | Automation Controller configuration                                     | string |

The "aap_controller_instance_configuration" variable has the following attributes:

|  Attributes  |                     Definition                 |  Type  |
|:------------:|:----------------------------------------------:|:------:|
|     name     | Automation Controller instance name            | string |
|   replicas   | Automation Controller instance replicas        | string |
|     route    | Automation Controller instance route           | string |

For example:

```yaml
aap_controller_instance_configuration:
  - name: aapctl-instance-1
    replicas: 3
    route: aapctl-console-1.apps.ocp4cluster.ocp4.cfernand.com
```

#### AAP Hub Variables

The following variables are defined:

|                Variable                |                               Definition                                |  Type  |
|:--------------------------------------:|:-----------------------------------------------------------------------:|:------:|
|          aap_hub_instance_path         | Path with manifests to install Automation Hub instance                  | string |
|     aap_hub_instance_configuration     | Automation Hub configuration                                            | string |

The "aap_controller_instance_configuration" variable has the following attributes:

|  Attributes  |                     Definition                 |  Type  |
|:------------:|:----------------------------------------------:|:------:|
|     name     | Automaton Hub instance name                    | string |
|     route    | Automation Hub instance route                  | string |

For example:

```yaml
aap_hub_instance_configuration:
  - name: aaphub-instance-1
    route: aaphub-console-1.apps.ocp4cluster.ocp4.cfernand.com
```
